/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/widget/src/master/LICENSE)
 */

'use strict';

var app = angular.module('widgetApp', [
  'ngRoute',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'LocalStorageModule',
  'AngularGM',
  'widgetApp.services',
  'widgetApp.properties',
  'services.config',
  'angulartics',
  'angulartics.google.analytics'
]);

app.config(['$routeProvider', '$analyticsProvider',
    function ($routeProvider, $analyticsProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      }).
      when('/login', {
        templateUrl: 'views/login.html',
        controller: 'Login'
      }).
      when('/feedback', {
        templateUrl: 'views/feedback.html',
        controller: 'Feedback'
      }).
      when('/collection/:slug', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      }).
      otherwise({
        redirectTo: '/'
      });
    $analyticsProvider.virtualPageviews(false);
  }]);

app.run(['$routeParams', '$location', 'widgetProperties', 'dropspot', 'configuration',
    function ($routeParams, $location, widgetProperties, dropspot, configuration) {
        dropspot.setUrl(configuration.API_PATH);
    }]);

app.run(['$rootScope', '$window', 'angulargmContainer', 'widgetProperties',
    function ($rootScope, $window, angulargmContainer, widgetProperties) {
        // add the widgetSize to the global scope
        $rootScope.overlayerWidth = angular.element('#widget').width();
        $rootScope.overlayerHeight = angular.element('#widget').height();

        // listen to widgetSize changes
        angular.element($window).bind('resize', function () {
            $rootScope.overlayerWidth = angular.element('#widget').width();
            $rootScope.overlayerHeight = angular.element('#widget').height();
            $rootScope.$apply('overlayerWidth');
            $rootScope.$apply('overlayerHeight');
        });

        // if the map is accessible we can use it
        var gmapPromise = angulargmContainer.getMapPromise('myMap');
        gmapPromise.then(function (gmap) {
            widgetProperties.setMap(gmap);
            $rootScope.$broadcast("gmap_loaded");
        });
    }]);

// initialize FB JDK and fire auth.statusChange event after the initialization is complete
app.run(['$rootScope', 'configuration', function ($rootScope, configuration) {
    window.fbAsyncInit = function () {
        FB.init({
            appId: configuration.FB_APP_ID,
            status:true,
            cookie:true,
            xfbml:true
        });

        FB.Event.subscribe('auth.statusChange', function(response) {
            $rootScope.$broadcast("fb_statusChange", {'status': response.status});
        });
    };

    (function (d) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));
}]);

app.directive('staticInclude', function($http, $templateCache, $compile) {
    return function(scope, element, attrs) {
        var templatePath = attrs.staticInclude;

        $http.get(templatePath, {cache: $templateCache}).success(function(response) {
            var contents = $('<div/>').html(response).contents();
            element.html(contents);
            $compile(contents)(scope);
        });
    };
});
