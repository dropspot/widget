/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/widget/src/master/LICENSE)
 */

'use strict';

angular.module('widgetApp')
  .directive('dropspotIphone', function () {
    return {
      templateUrl: '../templates/dropspotiphone.html',
      restrict: 'E',
      scope: { collection:'=' }
    }
  });
