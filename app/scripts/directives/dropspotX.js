/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/widget/src/master/LICENSE)
 */

'use strict';

angular.module('widgetApp')
  .directive('dropspotX', ['$location', 'widgetProperties', function ($location, widgetProperties) {
    return {
      template: '<a class="x" ng-click="goHome()"></a>',
      restrict: 'E',
      controller: ['$scope', function ($scope) {
        $scope.goHome = function () {
          $location.path('/collection/' + widgetProperties.getCollectionSlug());
        };
      }]
    };
  }]);
