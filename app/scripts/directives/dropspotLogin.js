/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/widget/src/master/LICENSE)
 */

'use strict';

angular.module('widgetApp')
  .directive('dropspotLogin', ['dropspot', function (dropspot) {
    return {
      templateUrl: '../templates/dropspotlogin.html',
      restrict: 'E',
      scope: {
        successCallback: '&successCallback',
        errorCallback: '&errorCallback'
      },
      controller: ['$scope', '$analytics', function ($scope, $analytics) {

        $scope.authView = 'login_welcome';
        $scope.errorMessage = '';

        $scope.toggle = function toggle(authView) {
          $scope.errorMessage = '';
          $scope.authView = authView;
        }


        /**
         * Log in user using dropspot service
         *
         * @param user
         */
        $scope.login = function (user) {

          // emit event track (with category and label properties for GA)
          $analytics.eventTrack('Dropspot Login Start', {
            category: 'Map Widget'
          });

          dropspot.login(user.name, user.password)
            .then(function (data) {

              // emit event track (with category and label properties for GA)
              $analytics.eventTrack('Dropspot Login Success', {
                category: 'Map Widget'
              });

              $scope.successCallback();
            }, function (reason) {

              // emit event track (with category and label properties for GA)
              $analytics.eventTrack('Dropspot Login Failure', {
                category: 'Map Widget'
              });

              $scope.errorMessage = 'Wrong username or password. Please try again.';
              $scope.errorCallback();
            });
        }


        /**
         * Signup and login user using dropspot service
         *
         * @param user
         */
        $scope.signup = function (user) {

          // emit event track (with category and label properties for GA)
          $analytics.eventTrack('Signup Start', {
            category: 'Map Widget'
          });

          dropspot.signup(user.name, user.password, user.email)
            .then(function (data) {

              // emit event track (with category and label properties for GA)
              $analytics.eventTrack('Signup Success', {
                category: 'Map Widget'
              });

              //after the user is created, sign the user in
              dropspot.login(user.name, user.password)
                .then(function (data) {
                  $scope.successCallback();
                }, function (error, status) {
                  console.log('Login failed. Reason: ' + error);
                  $scope.errorCallback();
                });
            }, function (error) {

              // emit event track (with category and label properties for GA)
              $analytics.eventTrack('Signup Failure', {
                category: 'Map Widget'
              });

              $scope.errorMessage = 'User with such name already exists';
              $scope.errorCallback();
            });
        }


        /**
         * Facebook login
         */
        $scope.fbLogin = function () {

          // emit event track (with category and label properties for GA)
          $analytics.eventTrack('FB Login Start', {
            category: 'Map Widget'
          });

          FB.login(function (response) {
            if (response.authResponse) {
              //after the user logs in to Facebook, sign user to dropspot
              console.log("Facebook login successful");
              var access_token = FB.getAuthResponse()['accessToken'];
              console.log('Access Token = ' + access_token);
              dropspot.fbLogin(access_token)
                .then(function (data) {

                  // emit event track (with category and label properties for GA)
                  $analytics.eventTrack('FB Login Success', {
                    category: 'Map Widget'
                  });

                  $scope.successCallback();
                }, function (error, status) {

                  // emit event track (with category and label properties for GA)
                  $analytics.eventTrack('FB Login Failure', {
                    category: 'Map Widget'
                  });

                  console.log('Dropspot login with Facebook access token failed. Reason: ' + error);
                  $scope.errorMessage = 'Facebook login was unsuccessful. Please try again.';
                  $scope.errorCallback();
                });
            } else {

                  // emit event track (with category and label properties for GA)
                  $analytics.eventTrack('FB Login Failure', {
                    category: 'Map Widget'
                  });

              $scope.errorMessage = 'Facebook login was unsuccessful. Please try again.';
              $scope.errorCallback();
            }
          }, {scope: 'email'});
        };
      }]
    };
  }]);
