/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/widget/src/master/LICENSE)
 */

'use strict';

var services = angular.module('widgetApp.properties', []);

services.factory('widgetProperties', function () {
  var spot_pixel_offset_x = 0;
  var spot_pixel_offset_y = 0;

  var collectionSlug = null;
  var profile = null;
  var subscriptions = null;
  var collection = null;
  var subscribed = null;

  var map = null;

  var spots = [];
  var current_spot_index = 0
  var current_spot = spots[ current_spot_index ];
  var dataLoaded = false;
  var spotCount = 0;

  return {
    getMap: function() {
      return map;
    },
    getCollection: function () {
      return collection
    },
    /**
     * Lazily sets and returns subscription status
     *
     * @returns {boolean}
     */
    isSubscribed: function () {
      if (subscribed === null || typeof(subscribed) !== "boolean") {
        subscribed = false;
        // if subscribed var is not set, set it based on profile subscriptions and collection slug
        if (collectionSlug !== null && profile !== null) {
          // if collection slug and profile, set subscribed value
          for (var i in subscriptions) {
            if (subscriptions[i].content_type == "collection") {
              if (subscriptions[i].content_object.slug === collectionSlug) {
                subscribed = true;
                break;
              }
            }
          }
        } else if (collectionSlug === null || profile === null) {
          //if collection slug or profile are not set, output error message and fallback to 'unsubscribed'
          console.log("Cannot check subscription status: collection slug or profile are undefined");
          subscribed = false;
        }
    }
    return subscribed;
  },

    getProfile: function () {
      return profile;
    },
    getSubscriptions: function () {
      return subscriptions;
    },
    getCollectionSlug: function () {
      return collectionSlug;
    },
    getOffset_x: function(){
      return spot_pixel_offset_x;
    },
    getOffset_y: function(){
      return spot_pixel_offset_y;
    },
    getSpots: function() {
      return spots;
    },
    getCurrentSpot: function(){
      return current_spot;
    },
    getCurrentSpotIndex: function(){
      return current_spot_index;
    },
    getMaxSpot: function(){
      return spots.length -1;
    },
    get_loadedData: function(){
      return dataLoaded;
    },
    getMax: function(){
      return spotCount;
    },
    setMap: function (aMap) {
      map = aMap;
    },
    setSubscribed: function(aSubscribed) {
      subscribed = aSubscribed;
    },
    setCollection: function (aCollection) {
      collection = aCollection;
    },
    setProfile: function (aProfile) {
      profile = aProfile;
    },
    setSubscriptions: function (aSubscriptions) {
      subscriptions = aSubscriptions;
    },
    setCollectionSlug: function (slug) {
      collectionSlug = slug;
    },
    set_loadedData: function(bool){
      dataLoaded = bool;
    },
    appendSpots: function(spot) {
      spots.push(spot);
      spotCount = spots.length -1;
    },
    setSpots: function(spotArray){
      spots = spotArray;
      spotCount = spots.length -1;
    },
    set_current_spot_index: function(index){
      current_spot_index = index;
    },
    setCurrentSpot: function(spot){
      current_spot = spot;
    },
    setOffset_x: function(offset_x){
      spot_pixel_offset_x = offset_x;
    },
    setOffset_y: function(offset_y){
      spot_pixel_offset_y = offset_y;
    }
  }
});
