/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/widget/src/master/LICENSE)
 */

'use strict';

var services = angular.module('widgetApp.services', []);

services.factory('dropspot', ['$http', '$cookies', '$q', 'localStorageService',
  function ($http, $cookies, $q, localStorageService) {
  var url = '',
    csrftoken = $cookies.csrftoken;

  return {
    /* Sets url of the service host */
    setUrl: function (newUrl) {
      url = newUrl;
    },

    /* Am I logged in? */
    isLogged: function () {
      return !!localStorageService.get('token');
    },

    /* Get collection */
    collection: function (slug) {
      var deferred = $q.defer();
      $http({
        method: 'GET',
        url: url + '/collections/' + slug + '/.json'
      }).success(function (data) {
          deferred.resolve(data)
        }).error(function (data, status) {
          deferred.reject(status);
        });
      return deferred.promise;
    },

  /**
   * Get spots
   *
   * @param slug - collection slug, empty if it no collection specified
   * @param link - optional parameter for filtering only spots that contain a link similar to it
   */
    spots: function (slug, link) {
      var deferred = $q.defer();
      var link_param = '';
      if(link) {
          link_param = '&fuzzy_link=' + link + '&t=0.85';
      }

      $http({
        method: 'GET',
        url: url + '/collections/' + slug + '/spots/' + '.json?page_size=1000' + link_param
      }).success(function (data) {
          deferred.resolve(data)
        }).error(function (data, status) {
          deferred.reject(status);
        });
      return deferred.promise;
    },

    /* Subscribe authenticated user to a collection */
    subscribe: function (slug) {
      var token = localStorageService.get('token');
      var deferred = $q.defer();
      $http({
        method: 'PUT',
        url: url + '/collections/' + slug + '/subscribe/.json',
        headers: {
          'X-CSRFToken': csrftoken,
          'Authorization': 'Token ' + token
        }
      }).success(function (data, status) {
          deferred.resolve({
            success: 'subscribe',
            status: status
          });
        }).error(function (data, status) {
          deferred.reject(status);
        });
      return deferred.promise;
    },

    /* Login and save session id in the cookie */
    loginWithSessionCookie: function (username, password) {
      var xsrf = $.param({login: "sergei", password: password, csrfmiddlewaretoken: csrftoken });
      var deferred = $q.defer();
      $http({
        method: 'POST',
        url: url + '/account/login/',
        data: xsrf,
        headers: {
          'X-CSRFToken': csrftoken,
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
        }
      }).success(function (data) {
          deferred.resolve({
            success: 'login',
            username: data.username
          });
        }).error(function (data, status) {
          console.log('Login failure. Status: ' + status);
          deferred.reject(data);
        });
      return deferred.promise;
    },

    /**
     * Uses username and password to
     * obtain the token and store it as 'token' with the username as
     * 'username' in the local storage of the browser
     *
     * @param username
     * @param password
     * @returns {Promise.promise|*}
     */
    login: function (username, password) {
      var deferred = $q.defer();
      $http({
        method: 'POST',
        url: url + '/token/.json',
        data: {
          username: username,
          password: password
        },
        headers: {
          'X-CSRFToken': csrftoken
        }
      }).success(function (data) {

          localStorageService.add("token", data.token);
          localStorageService.add("username", username);

          deferred.resolve({
            success: 'login',
            username: data.username
          });
        }).error(function (error, status) {
          deferred.reject({
            status: status,
            error: error
          });
        });
      return deferred.promise;
    },


    /**
     * Uses Facebook access token
     * obtain the token and store it as 'token' with the username as
     * 'username' in the local storage of the browser
     *
     * The method submits POST request with 'Content-Type' : 'application/x-www-form-urlencoded'
     * instead of 'applicatino/json/charset=UTF-8' to workaround Issue #6
     *
     * @param access_token
     * @returns {Promise.promise|*}
     */
    fbLogin: function (access_token) {
     var deferred = $q.defer();
      var xsrf = $.param({access_token: access_token});

      //using a form post to workaround Issue #6
      $http({
        method: 'POST',
        url: url + '/token/.json',
        data: xsrf,
/*
        {
          access_token: access_token
        },
*/
        headers: {
          'X-CSRFToken': csrftoken,
          'Content-Type' : 'application/x-www-form-urlencoded'
        }
      }).success(function (data) {

          localStorageService.add("token", data.token);
          localStorageService.add("username", data.username);

          deferred.resolve({
            success: 'fbLogin',
            username: data.username
          });
        }).error(function (error, status) {
          deferred.reject({
            status: status,
            error: error
          });
        });
      return deferred.promise;
    },

    /*
     Sign up a new user
     */
    signup: function (username, password, email) {
      var deferred = $q.defer();
      $http({
        method: 'POST',
        url: url + '/users/.json',
        data: {
          username: username,
          password: password,
          email: email
        },
        headers: {
          'X-CSRFToken': csrftoken
        }
      }).success(function (data) {
          deferred.resolve({
            success: 'signup',
            username: data.username
          });
        }).error(function (error, status) {
          deferred.reject({
            status: status,
            error: error
          });
        });
      return deferred.promise;
    },

    /*
     Getting and saving in a service variable the profile info about the user.
     Fails with status code 401 if no valid token is provided.
     */
    profile: function () {
      var token = localStorageService.get('token');
      var deferred = $q.defer();
      $http({
        method: 'GET',
        url: url + '/profile/.json',
        headers: {
          'X-CSRFToken': csrftoken,
          'Authorization': 'Token ' + token
        }
      }).success(function (data) {
          deferred.resolve(data);
        }).error(function (error, status) {
          deferred.reject({
            status: status,
            error: error
          });
        });
      return deferred.promise;
    },

    /*
     Getting and saving in a service variable the profile info about the user.
     Fails with status code 401 if no valid token is provided.
     */
    subscriptions: function (url) {
      var token = localStorageService.get('token');
      var deferred = $q.defer();
      $http({
        method: 'GET',
        url: url,
        headers: {
          'X-CSRFToken': csrftoken,
          'Authorization': 'Token ' + token
        }
      }).success(function (data) {
          deferred.resolve(data);
        }).error(function (error, status) {
          deferred.reject({
            status: status,
            error: error
          });
        });
      return deferred.promise;
    }
  }
}]);