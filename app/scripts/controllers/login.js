/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/widget/src/master/LICENSE)
 */

'use strict';

angular.module('widgetApp')
  .controller('Login', ['$rootScope', '$scope', '$location', 'dropspot', 'widgetProperties',
    function ($rootScope, $scope, $location, dropspot, widgetProperties) {
    $scope.collection = widgetProperties.getCollection();

    $scope.successCallback = function () {
      dropspot.profile()
        .then(function (profile) {
          //save profile in global scope
          widgetProperties.setProfile(profile);
          //check if the user is subscribed
        },function (error) {
          //error getting profile info
          console.log("Error getting user's profile")
        },function (update) {
          //processing the request
        }).then(function () {
          if (!widgetProperties.isSubscribed()) {
            //subscribe the user
            dropspot.subscribe(widgetProperties.getCollectionSlug(), $scope)
              .then(function (data) {
                widgetProperties.setSubscribed(true);

                //go to feedback screen
                $location.path('/feedback/');
              }, function (error, status) {
                console.log("Subscription failed")
                $location.path('/collection/' + widgetProperties.getCollectionSlug());
              });
          } else
            $location.path('/collection/' + widgetProperties.getCollectionSlug());
        })
    }

    $scope.errorCallback = function () {
      //$location.path('/');
    }
  }]);
