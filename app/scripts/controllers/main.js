/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/widget/src/master/LICENSE)
 */

'use strict';

var app = angular.module('widgetApp');

app.controller('MainCtrl', ['$rootScope', '$routeParams', '$scope',
    '$timeout', '$http', '$cookies', '$location', 'dropspot',
    'widgetProperties', '$analytics',
    function($rootScope, $routeParams, $scope,
        $timeout, $http, $cookies, $location, dropspot,
        widgetProperties, $analytics) {

        if ($routeParams.slug != null) {
            widgetProperties.setCollectionSlug($routeParams.slug);
        } else {
            widgetProperties.setCollectionSlug('');
        }

        if ($routeParams.style === undefined) {
            $scope.style = {
                'name': 'default',
                'auth': true,
                'map_interact': true,
                'link_filter': false
            };
        } else {
            $scope.style = JSON.parse($routeParams.style);
        }

        if ($scope.style.name == 'bild-mobile') {
            $scope.margin_bottom_map = 50;
            $scope.margin_right_map = 0;

            $scope.marker_style_active = '/images/bild_custom_marker.png';
            $scope.marker_style_inactive = '/images/bild_custom_marker_inactive.png';

        } else if ($scope.style.name == 'bild-custom') {
            $scope.margin_bottom_map = 50;
            $scope.margin_right_map = 0;
            $scope.marker_style_active = '/images/bild_custom_marker.png';
            $scope.marker_style_inactive = '/images/bild_custom_marker_inactive.png';

        }else{
            $scope.margin_bottom_map = 0;
            $scope.margin_right_map = 0;
            $scope.marker_style_active = '/images/drop_icon_highlight.png';
            $scope.marker_style_inactive = '/images/drop_icon_default.png';
        }

        // update Center with pixel offset
        $scope.updateCenter = function(lat, lng) {

            // get the MapScale
            var scale = Math.pow(2, widgetProperties.getMap().getZoom());
            // Lat/Lng as MapPoint
            var current = new google.maps.LatLng(lat, lng);
            var current_coordinate = widgetProperties.getMap().getProjection().fromLatLngToPoint(current);
            // Offset as MapPoint
            var nuPixel = new google.maps.Point(widgetProperties.getOffset_x() / scale,
                widgetProperties.getOffset_y() / scale);

            //wrapping center update in $timeout to resolve issues with refreshing map
            //when switching back from login view.
            //TODO: find out why it helps
            $timeout(function() {
                $scope.center = widgetProperties.getMap().getProjection().fromPointToLatLng(
                    new google.maps.Point(current_coordinate.x - nuPixel.x,
                        current_coordinate.y - nuPixel.y));

                // redraw the map
                $scope.$broadcast('gmMapResize', 'myMmap');
            })
        };

        // if there is something there already, set it
        $scope.spots = widgetProperties.getSpots();
        $scope.current_spot = widgetProperties.getCurrentSpot();
        $scope.current_spot_index = widgetProperties.getCurrentSpotIndex();
        var max = widgetProperties.getMax();

        $scope.showJumbo = false;
        $scope.showJumbotron = function() {
            $scope.showJumbo = true;
        };
        $scope.hideJumbotron = function() {
            $scope.showJumbo = false;
        };

        $scope.collection = widgetProperties.getCollection();

        var map_options = {
            center: new google.maps.LatLng(48, 48),
                zoom: 15,
                disableDefaultUI: true,
                mapTypeControl: false,
                draggable: false,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoomControl: false,
                disableDoubleClickZoom: true
        };

        if($scope.style.map_interact) {
            map_options.zoom = 14;
            map_options.draggable = true;
            map_options.zoomControl = true;
            map_options.zoomControlOptions = {
                    style: google.maps.ZoomControlStyle.DEFAULT,
                    position: google.maps.ControlPosition.LEFT_CENTER
            };
        }
        // Set map & marker options
        $scope.map_options = {
            map: map_options,
            default_marker: {
                icon: $scope.marker_style_inactive
            },
            highlight_marker: {
                icon: $scope.marker_style_active
            }

        };

        //check if collection was already retrieved from the server
        if (widgetProperties.getCollection() === null) {
            dropspot.collection(widgetProperties.getCollectionSlug())
                .then(function(data) {
                    widgetProperties.setCollection(data);
                    $scope.collection = data;

                    if ($scope.collection.created_by.primary_avatar === null) {
                        $scope.collection.created_by.primary_avatar = '../images/drpspt-logo.png';
                    }

                }, function() {
                    console.log("Error getting collection");
                }).then(function() {
                    // get url of the widget parent
                    var parentUrl = document.referrer.replace(/.*?:\/\//g, "").replace(/\/$/g, "");
                    var linkFilter;
                    if($scope.style.link_filter) {
                        linkFilter = parentUrl;
                    }

                    dropspot.spots(widgetProperties.getCollectionSlug(), linkFilter)
                        .then(function (data) {
                            if (!linkFilter) {
                                // if no link filter was used, rearrange the spots to have the spots
                                // with at least one link that is identical to the link of the parent docoment
                                // TODO: implement link comparison by using a string similarity algorithm
                                angular.forEach(data.results, function (spot) {
                                    // if one of the spot's link is the same as the link of the parent window,
                                    // add spot to the top of the spots array
                                    var unshiftFlag = false;
                                    for(var i=0; i< spot.content_links.links.length; i++)
                                    {
                                       if (parentUrl === spot.content_links.links[i].replace(/.*?:\/\//g, "").replace(/\/$/g, "")) {
                                           $scope.spots.unshift(spot);
                                           unshiftFlag = true;
                                           break;
                                        }
                                    }
                                    if(!unshiftFlag) {
                                        $scope.spots.push(spot);
                                    }
                                });
                                setDefaults();
                            } else if (linkFilter && data.count === 0) {
                                // if no spots with similar links were returned
                                // repeat the request without link similarity and show all the spots
                                dropspot.spots(widgetProperties.getCollectionSlug())
                                    .then(function (data) {
                                        $scope.spots = data.results;
                                        setDefaults();
                                    });
                            } else {
                                $scope.spots = data.results;
                                setDefaults();
                            }

                            // Set the defaults
                            function setDefaults() {
                                $scope.current_spot_index = 0;
                                max = $scope.spots.length - 1;
                                $scope.current_spot = $scope.spots[0];
                                $scope.mapCtrMultiSpot = ($scope.spots.length > 1);
                                widgetProperties.setCurrentSpot($scope.current_spot);
                                widgetProperties.setSpots($scope.spots);
                                widgetProperties.set_current_spot_index(0);
                            }
                        })
                })
        } else {
            $scope.updateCenter($scope.current_spot.latitude, $scope.current_spot.longitude);
        }


        if ($scope.style.auth === true) {
            //check if user authenticated
            if (dropspot.isLogged()) {
                //check if the profile is already there
                if (widgetProperties.getProfile() === null) {
                    //if not, make a call for it and store it in global Scope
                    processLoggedUser();
                } else {
                    //if the profile was there, immediately check if the user is subscribed
                    setSubscribeButtonText()
                }
            } else {
                //user is not logged in
                setSubscribeButtonText()
            }
        }

        // Handle fluidWindowSize
        $scope.width = $(window).width();
        $scope.height = $(window).height() - $scope.margin_bottom_map;


        // to get a better marker center listen to the detail div position / 2
        $scope.factor_x = Math.round($scope.width / 4);
        $scope.factor_y = Math.round($scope.height / 4);


        $rootScope.$on("gmap_loaded", function() {
            google.maps.event.addListener(widgetProperties.getMap(), 'projection_changed', function() {
                if ($scope.current_spot !== undefined && widgetProperties.getMap().getProjection() !== undefined) {
                    $scope.updateCenter($scope.current_spot.latitude, $scope.current_spot.longitude);
                }
            });
        });

        $scope.$watch('widgetProperties.getMap().getProjection() + current_spot', function() {
            if ($scope.current_spot !== undefined && widgetProperties.getMap().getProjection() !== undefined) {
                $scope.updateCenter($scope.current_spot.latitude, $scope.current_spot.longitude);
            }
        }, true);

        // Change the marker for the current viewed spot
        $scope.getCurrentSpot = function() {
            angular.forEach($scope.spots, function(spot) {
                if ($scope.current_spot.slug === spot.slug) {
                    spot.marker = angular.extend($scope.map_options.highlight_marker);
                } else
                    spot.marker = angular.extend($scope.map_options.default_marker);
            });
            $scope.$broadcast('gmMarkersRedraw', 'spots');
        };

        $scope.$watch('current_spot', function() {
            $scope.getCurrentSpot();
        }, true);


        //process event that is fired when FB is initialized
        //see FB.init() in app.js
        $rootScope.$on("fb_statusChange", function(event, args) {
            if (args.status == 'connected' && dropspot.isLogged() === false) {
                var access_token = FB.getAuthResponse()['accessToken'];
                dropspot.fbLogin(access_token)
                    .then(function() {
                        processLoggedUser();
                    }, function() {
                        console.log('Error authenticating against dropspot after FB is initialized and status is' +
                            ' "connected"');
                    })
            }
        });

        // Btn action NEXT
        $scope.next = function() {
            if (max > 0) {
                $scope.current_spot_index = setCurrentSpotByIndex(++$scope.current_spot_index);

                // emit event track (with category and label properties for GA)
                $analytics.eventTrack('Scroll map next', {
                    category: 'Map Widget'
                });
            } else {
                //do nothing
            }
        };

        // Btn action PREV
        $scope.prev = function() {
            if (max > 0) {
                $scope.current_spot_index = setCurrentSpotByIndex(--$scope.current_spot_index);

                // emit event track (with category and label properties for GA)
                $analytics.eventTrack('Scroll map prev', {
                    category: 'Map Widget'
                });
            } else {
                //do nothing
            }
        };

        /**
         * Click on a marker
         * @param spot
         */
        $scope.markerClick = function(spot) {
            var index = $scope.spots.indexOf(spot);
            $scope.current_spot_index = setCurrentSpotByIndex(index);

            // emit event track (with category and label properties for GA)
            $analytics.eventTrack('Spot click', {
                category: 'Map Widget'
            });
        };

        /**
         * Helper function for setting $scope.spots[index] spot as active
         * @param index
         * @returns {*}
         */
        function setCurrentSpotByIndex(index) {
            if (index < 0)
                index = max;
            if (index > max)
                index = 0;

            $scope.current_spot = $scope.spots[index];
            $scope.updateCenter($scope.current_spot.latitude, $scope.current_spot.longitude);

            return index;
        }

        // rerender map when you come back to the mapView
        $scope.$watch('center', function() {
            if ($scope.current_spot)
                $rootScope.$broadcast('gmMapResize', 'myMap');
        }, true);

        // Watch the id=#widget DIV on windowchange
        $scope.$watch('overlayerWidth', function(newVal) {
            if (newVal) {
                $scope.width = $(window).width() - $scope.margin_right_map;

                $scope.factor_x = Math.round($scope.width / 8);
                $scope.factor_y = Math.round($scope.height / 8);

                if ($scope.current_spot !== undefined)
                    $scope.updateCenter($scope.current_spot.latitude, $scope.current_spot.longitude);

                // its dirty but ok for the moment
                if ($scope.width > 480) {
                    if($scope.style.name === 'bild-custom'){
                        widgetProperties.setOffset_y($scope.factor_y);
                        widgetProperties.setOffset_x(0);
                    }else if($scope.style.name === 'default'){
                        widgetProperties.setOffset_x(-1.5 * $scope.factor_x);
                        widgetProperties.setOffset_y(0);
                    }

                } else {
                    //console.log("HORIZONTAL MODE");
                    //widgetProperties.setOffset_x(0);
                    if($scope.style.name === 'default'){
                        widgetProperties.setOffset_y(-1.5 * $scope.factor_y);
                        widgetProperties.setOffset_x(0);
                    }

                }
            }
        });

        $scope.$watch('overlayerHeight', function(newVal) {
            if (newVal) {
                $scope.height = $(window).height() - $scope.margin_bottom_map;

                $scope.factor_x = Math.round($scope.width / 8);
                $scope.factor_y = Math.round($scope.height / 8);

                if ($scope.current_spot !== undefined)
                    $scope.updateCenter($scope.current_spot.latitude, $scope.current_spot.longitude);

                // its dirty but ok for the moment
                if ($scope.width > 480) {
                    widgetProperties.setOffset_x(-1 * $scope.factor_x + (2 * $scope.margin_right_map));
                    widgetProperties.setOffset_y($scope.factor_x);
                } else {
                    widgetProperties.setOffset_x(0);
                    widgetProperties.setOffset_y(-1 * $scope.factor_y);
                }
            }
        });

        /**
         * [Subscribe] button action
         */
        $scope.subscribe = function() {
            if (dropspot.isLogged() == true && widgetProperties.isSubscribed() == false) {
                //subscribe user

                // emit event track (with category and label properties for GA)
                $analytics.eventTrack('Click Subscribe', {
                    category: 'Map Widget'
                });

                dropspot.subscribe(widgetProperties.getCollectionSlug(), $scope)
                    .then(function() {
                        widgetProperties.setSubscribed(true);
                        //go to feedback screen
                        $location.path('/feedback/');
                    }, function() {
                        console.log("Subscription failed")
                    });
            } else if (dropspot.isLogged() == true && widgetProperties.isSubscribed() == true) {
                //open the iTunes store
            } else if (dropspot.isLogged() == false) {
                //go to login screen

                // emit event track (with category and label properties for GA)
                $analytics.eventTrack('Click Log In', {
                    category: 'Map Widget'
                });

                $location.path('/login/');
            }
        };

        /**
         * Sets text on [subscribe] button depending on whether the user is logged in and subscribed
         */
        function setSubscribeButtonText() {
            if (!dropspot.isLogged())
                $scope.subscribeButtonText = "Log In!";
            else if (!widgetProperties.isSubscribed())
                $scope.subscribeButtonText = "Subscribe!";
            else
                $scope.subscribeButtonText = "Already subscribed";
        }

        /**
         * Loads profile and saves in global scope is widgetProperties service
         */
        function processLoggedUser() {
            dropspot.profile()
                .then(function(profile) {
                    //save profile in global scope
                    widgetProperties.setProfile(profile);
                    //check if the user is subscribed
                    dropspot.subscriptions(profile.subscriptions)
                        .then(function(subsriptions) {
                            widgetProperties.setSubscriptions(subsriptions.results);

                            //setting the text for [subscribe] button
                            setSubscribeButtonText()
                        }, function() {
                            //error getting user's subscriptions
                            console.log("Error getting user's profile")
                        })
                }, function() {
                    //error getting profile info
                    console.log("Error getting user's profile")
                });
        }
    }
]);
