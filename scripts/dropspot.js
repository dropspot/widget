/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT (https://bitbucket.org/dropspot/widget/src/master/LICENSE)
 */

var WIDGET_PATH = '@@WIDGET_PATH';
var BILD_MOBILE_WIDGET_PATH = '@@BILD_MOBILE_WIDGET_PATH';
var BILD_MOBILE_1_WIDGET_PATH = '@@BILD_MOBILE_1_WIDGET_PATH';

var STYLE = {
    'default': {
        'name': 'default',
        'auth': true,
        'map_interact': true,
        'link_filter': false,
        'height': {
            'wide': '286px',
            'tall': '570px'
        }
    },
    'app-link': {
        'name': 'app-link',
        'auth': false,
        'map_interact': true,
        'link_filter': false,
        'height': {
            'wide': '286px',
            'tall': '570px'
        }
    },
    'bild-beta': {
        'name': 'bild-beta',
        'auth': false,
        'map_interact': true,
        'link_filter': true,
        'height': {
            'wide': '286px',
            'tall': '570px'
        }
    },
    'bild-custom': {
        'name': 'bild-custom',
        'auth': false,
        'map_interact': true,
        'link_filter': true,
        'height': {
            'wide': '286px',
            'tall': '570px'
        }
    },
        'ts-beta': {
            'name': 'ts-beta',
            'auth': false,
            'link_filter': true,
            'height': {
                'wide': '286px',
                'tall': '570px'
            }
        },
    'bild-mobile': {
        'name': 'bild-mobile',
        'auth': false,
        'map_interact': false,
        'link_filter': true,
        'height': {
            'wide': '340px',
            'tall': '340px'
        }
    },
    'bild-mobile-1': {
        'name': 'bild-mobile-1',
        'auth': false,
        'map_interact': false,
        'link_filter': true,
        'height': {
            'wide': '340px',
            'tall': '340px'
        }
    }
};

// using dropspot.widget as a namespace
var dropspot = dropspot || {};
dropspot.widget = dropspot.widget || {};

/**
 * Using anonymous self-executing function to achieve code isolation
 */
(function() {
    var widgetStyle = STYLE['default'];

    loadWidgetIFrame();

    window.onresize = function() {
        var widgets = document.getElementsByClassName('dropspot-collection');
        for (var i = 0; i < widgets.length; ++i) {
            var widget = widgets[i];

            setWidgetHeight(widget);
        }
    };

    /**
     * Adjusts height of the widget according to parameters stored in STYLE
     * @param widgetDiv
     */
    function setWidgetHeight(widgetDiv) {
        var widgetWidth = 0;


        //getting the value of computed width of the iFrame
        // The DOM Level 2 CSS way
        //
        if ('getComputedStyle' in window) {
            widgetWidth = getComputedStyle(widgetDiv, null).getPropertyValue("width");
        }
        // The IE way
        //
        else if ('currentStyle' in widgetDiv) {
            widgetWidth = widget.currentStyle.getPropertyValue("width");
        }

        widgetWidth = parseInt(widgetWidth, 10);
        if (widgetWidth < 480) {
            widgetDiv.style.height = widgetStyle.height.tall;
        } else if (widgetWidth >= 480) {
            widgetDiv.style.height = widgetStyle.height.wide;
        }
    }

    /**
     * Create the iFrame with the widget
     */
    function loadWidgetIFrame() {
        var widgetDivs = document.getElementsByClassName('dropspot-collection'),
            slug = '',
            widgetPath = '',
            iFrame,
            widgetDiv;

        for (var i = 0; i < widgetDivs.length; ++i) {
            widgetDiv = widgetDivs[i];
            if (widgetDiv.getAttribute("data-style") && STYLE[widgetDiv.getAttribute("data-style")] !== undefined) {
                widgetStyle = STYLE[widgetDiv.getAttribute("data-style")];
            }
            slug = widgetDiv.getAttribute('data-slug');
            if (widgetStyle.name == 'bild-mobile') {
                widgetPath = BILD_MOBILE_WIDGET_PATH + '?collection=' + slug + '&article=' + document.URL ;
            } else if (widgetStyle.name == 'bild-mobile-1') {
                widgetPath = BILD_MOBILE_1_WIDGET_PATH + '?collection=' + slug;

            }
            else {
                widgetPath = WIDGET_PATH + '/#/collection/' + slug + '?style=' + JSON.stringify(widgetStyle);
            }
            if (!widgetDiv.hasChildNodes()) {
                iFrame = document.createElement('iframe');
                iFrame.frameBorder = 0;
                iFrame.width = "100%";
                iFrame.height = "100%";
                iFrame.scrolling = "no";
                iFrame.setAttribute("src", widgetPath);
                widgetDiv.appendChild(iFrame);

                setWidgetHeight(widgetDiv);
            }
        }
    }
})();
