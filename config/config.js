/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT (https://bitbucket.org/dropspot/widget/src/master/LICENSE)
 */

'use strict';

angular.module('services.config', [])
  .constant('configuration', {
    WIDGET_PATH: '@@WIDGET_PATH',
    API_PATH: '@@API_PATH',
    FB_APP_ID: '@@FB_APP_ID'
  });
