'use strict';

describe('Directive: drospotX', function () {

  // load the directive's module
  beforeEach(module('widgetApp'));

  var element,
    scope,
    location;

  beforeEach(inject(function ($rootScope, $location) {
    scope = $rootScope.$new();
    location = $location;
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<drospot-x></drospot-x>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('<a class="x" ng-click="goHome()"></a>');
  }));

  it('should redirect to "/" when clicked on', inject(function ($compile) {
    element = angular.element('<drospot-x></drospot-x>');
    element = $compile(element)(scope);
    element[0].click();
    expect(location.path()).toBe('/');
  }));
});
