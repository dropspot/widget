'use strict';

describe('Directive: dropspotLogin', function () {

  // load the directive's module
  beforeEach(module('widgetApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<dropspot-user-auth></dropspot-user-auth>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the dropspotLogin directive');
  }));
});
