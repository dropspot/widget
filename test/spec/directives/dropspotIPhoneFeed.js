'use strict';

describe('Directive: dropspotIPhoneFeed', function () {

  // load the directive's module
  beforeEach(module('widgetApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<dropspot-i-phone-feed></dropspot-i-phone-feed>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the dropspotIPhoneFeed directive');
  }));
});
