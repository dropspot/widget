'use strict';

describe('Controller: Feedback', function () {

  // load the controller's module
  beforeEach(module('widgetApp', [
  'ngRoute',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'LocalStorageModule',
  'AngularGM',
  'widgetApp.services'
]));

  var Feedback,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    Feedback = $controller('Feedback', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
